Afin de substituer MongoDB, j'ai opté pour la solution cloud d'Amazon Web Services "DynamoDB", mon choix s'est orienté vers cette
solution car il s'agit d'une part d'une solution gratuite (jusqu'à un certain quota) mais qui respecte également les contraintes
qu'impose un système de conversion de vidéos. En effet, cette base de données non relationnelle est extrêmement scalable, elle fournit
une reactivité constante de quelques millisecondes et ce, quelque soit l'échelle. 
DynamoDB est également une solution qui est entièrement gérée (sécurité, mise à l'echelle, failover). 
Petit plus: j'ai également opté pour cette solution car c'est uje l'ai déjà utilisé lors d'un projet professionnel et 
la durée de mise en place de la solution en a donc été réduite.




Vous trouverez le diagramme de composants dans le dossier "diagram".