
import logging
import boto3
import ffmpy
import time
import os
import websocket
import json
import ssl
import botocore

logging.basicConfig(format='%(asctime)s - %(levelname)s: %(message)s', level=logging.DEBUG)
#  ffmpeg -i Game.of.Thrones.S07E07.1080p.mkv -vcodec mpeg4 -b 4000k -acodec mp2 -ab 320k converted.avi


class VideoConversion(object):
	def __init__(self, _config_):
		self._config_=_config_
		#Création d'une session boto3 (librairie Amazon Web Service avec les identifiants IAM)
		self.session = boto3.Session(
			aws_access_key_id= self._config_.get_video_conversion_key_id(),
			aws_secret_access_key= self._config_.get_video_conversion_secret_access_id(),
			region_name= self._config_.get_video_conversion_region()
		)
		self.s3 = self.session.resource('s3')
		self.s3Client = boto3.client('s3')
		self.dynamodb = self.session.resource('dynamodb')
		self.bucket = self.s3.Bucket(self._config_.get_video_conversion_bucket())
		self.table = self.dynamodb.Table(_config_.get_video_conversion_collection())



	def find_one(self):
		conversion = self.video_conversion_collection.find_one()
		uri = conversion['originPath']
		id = conversion['_id']
		logging.info('id = %s, URI = %s',  id, uri  )
		ff = ffmpy.FFmpeg(
				inputs={uri: None},
				outputs={'converted.avi' : '-y -vcodec mpeg4 -b 4000k -acodec mp2 -ab 320k' }
			)
		logging.info("FFMPEG = %s", ff.cmd)
		# ff.run()
		self.video_conversion_collection.update({'_id' : id}, { '$set' : {'targetPath' : 'converted.avi'}})
		self.video_conversion_collection.update({'_id' : id}, { '$set' : {'tstamp' : time.time()  }})

	#Méthode en charge de la conversion du fichier
	def convert(self, _id_, _uri_):
		#Telecharge la video depuis le bucket amazon s3
		try:
			self.s3.Bucket(self._config_.get_video_conversion_bucket()).download_file(_uri_,_uri_)
		except botocore.exceptions.ClientError as e:
			if e.response['Error']['Code'] == "404":
				print("The object does not exist.")
			else:
				raise

		#Converti la video
		convert = _uri_.replace(".mkv", "-converted.avi")
		path = convert
		logging.info('ID = %s, URI = %s —› %s',  _id_, _uri_ , convert )
		ff = ffmpy.FFmpeg(
				inputs={_uri_: None},
				outputs={convert : '-y -vcodec mpeg4 -b 4000k -acodec mp2 -ab 320k' }
			)
		logging.info("FFMPEG = %s", ff.cmd)

		#Mock de FFMPEG
		os.rename(_uri_,convert)
		#ff.run()
		logging.info("PATHIO: %s", path)

		#Upload du fichier converti sur amazon s3
		self.s3Client.upload_file(path,self._config_.get_video_conversion_bucket(),path)

		#Suppression du fichier converti sur le système local
		os.remove(path)
		#for d in self.video_conversion_collection.find():
		#    logging.info(d)

		#Mise à jour de la table video_conversion en mettant le statut de conversion à true
		self.table.update_item(
			Key={
				'uuid': _id_
			},
			UpdateExpression='SET targetPath = :val1, converted = :val2',
			ExpressionAttributeValues={
				':val1': convert,
				':val2': 'true'
			}
		)
		

