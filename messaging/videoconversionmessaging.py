
from threading import Thread
import logging
import json
import queue
from google.cloud import pubsub_v1
import time

logging.basicConfig(format='%(asctime)s - %(levelname)s: %(message)s', level=logging.DEBUG)

class VideoConversionMessaging(Thread):
    def __init__(self, _config_, converting_service):
        Thread.__init__(self)
        #Création d'un suscriber à la plateforme Google Pub/Sub
        self.subscriber = pubsub_v1.SubscriberClient()
        self.project_id = _config_.get_pubsub_topic_id()
        self.sub_name = _config_.get_pubsub_subscription_name()
        self.subscription_path = self.subscriber.subscription_path(
            self.project_id, self.sub_name)

        #Méthode de callback appelée lorsqu'un nouvel élément est ajouté dans la queue associé au sub_name
        def callback(message):
            print('Received message: {}'.format(message))
            self._on_message_(message.data)
            message.ack()

        self.subscriber.subscribe(self.subscription_path, callback=callback)
        self.converting_service = converting_service
        self.rendez_vous = queue.Queue(1)
        self.pause = queue.Queue(1)
        self.start()

    def run(self):
        #Lis dans la queue en permanence, sur une intervalle de temps de 5 secondes
        while True :
            print('Listening for messages')
            time.sleep(5)


    def on_message(self, channel, method_frame, header_frame, body):
        logging.info(body)
        logging.info('URI = %s', body.decode())
        convert_request = json.loads(body.decode())
        logging.info(convert_request)
        self.converting_service.convert(convert_request["id"], convert_request['originPath'])

    #Méthode appelé par la méthode "callback" et qui demande la conversion du fichier à la classe VideoConversion
    def _on_message_(self,  body):
        logging.info(body)
        logging.info('URI = %s', body.decode())
        convert_request = json.loads(body.decode())
        logging.info(convert_request)
        self.converting_service.convert(convert_request["id"], convert_request['originFilePath'])



    def stop_consuming(self):
        logging.info("Stops consuming on message bus")
        # self.channel.stop_consuming()
        self.consuming = "_IDLE_"

    def start_consuming(self):
        logging.info("Starts consuming on message bus")
        #self.channel.start_consuming()
        # self.rendez_vous.put("_CONSUMING_")
        self.consuming = "_CONSUMING_"
        # self.start()

    def is_consuming(self):
        return self.consuming
