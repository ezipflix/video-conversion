import yaml
import logging

logging.basicConfig(format='%(asctime)s - %(levelname)s: %(message)s', level=logging.DEBUG)


class Configuration(object):
    def __init__(self):
        self.configuration_file = "/Users/nassimhammadi/Desktop/archiDistribuée/git2/video-conversion/application.yml" # Euuuuuurk !
        self.configuration_data = None

        f = open(self.configuration_file, 'r')
        self.configuration_data = yaml.load(f.read())
        f.close()

   
    def get_database_host(self):
        return self.configuration_data['spring']['data']['mongodb']['host']

    def get_pubsub_topic_id(self):
        return self.configuration_data['google']['pubsub']['topic_id']

    def get_pubsub_subscription_name(self):
        return self.configuration_data['google']['pubsub']['subscription_name']

    def get_video_conversion_collection(self):
        return self.configuration_data['amazon']['data']['mongodb']['collections']['video-conversions']

    def get_video_conversion_bucket(self):
        return self.configuration_data['amazon']['s3']['bucketname']

    def get_video_conversion_region(self):
        return self.configuration_data['amazon']['s3']['region']

    def get_video_conversion_secret_access_id(self):
        return self.configuration_data['amazon']['s3']['secret-access-id']

    def get_video_conversion_key_id(self):
        return self.configuration_data['amazon']['s3']['key-id']

